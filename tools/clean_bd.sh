
#
# this cleans design_1.tcl so that it only contains what is needed for project recreation
#

DESIGN1=`find ./ -iname design_1.tcl`

echo "create_bd_design \"design_1\""

# cut all the stuff between two current_bd_instance calls
cat $DESIGN1 | sed 's_^ *__g' | grep ^current_bd_instance -A 1000 | tail -n +2 | grep ^current_bd_instance -B 1000 | head -n -2

echo "save_bd_design"
echo "generate_target design_1.bd"

